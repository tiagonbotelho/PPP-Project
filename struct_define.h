#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <conio.h>
#include <windows.h>
#define MAX_NAME 101 /*Defines/includes*/
#define LAVAGEM 1
#define MANUTENCAO 0
#define NUMERO_MENU 101
#define DMHM 3
#define ANO 5
#define TELE 50
/*Estruturas*/
typedef struct{
int dia, mes, ano, hora, minutos;
}Data;

typedef struct{
char nome[MAX_NAME];
Data horarioi;
Data horariof;
long telemovel;
int tipo;
}Marcacao;

typedef struct lnode2 *Listainfo;
typedef struct lnode2{
    Marcacao  info;
    Listainfo next;
    Listainfo prev;
    }Li_node;


/*---------------------------------*/
/*Funcoes*/

Listainfo cria_lista(void);
void insere_lista(Listainfo Lista,Marcacao it);
int verifica_data(Marcacao data1, Marcacao data2);
void procura_lista(Listainfo lista, Listainfo *ant, Marcacao data);
void elimina_lista(Listainfo ListaD, Marcacao it);
int compara_hora(Data hora1, Data hora2);
int compara_dia(Marcacao data1,Marcacao data2);
int compara_reserva(Marcacao reserva1, Marcacao reserva2);
void modifica_hora(Marcacao *hora);
int disponibilidade(Marcacao horafixa, Marcacao horanova);
int VerificaPossibilidade(Listainfo lista,Marcacao nova);
void imprime_lista(Listainfo lista);
void numera_print(Listainfo Lista);
Marcacao Procura_listanumerada(Listainfo Lista,int n);
void encaixa_prereservas(Listainfo PReserva, Marcacao reserva_eliminada, Listainfo Lista);
void imprime_reserva(Marcacao *marcacao);
int imprimepnome(Listainfo Lista,Marcacao pessoa);
/*void Temporario(Listainfo lista);*/

/*Funcoes Menus.c*/
void Menu_geral(Listainfo Reserva, Listainfo PReserva);
void Menu_reserva(Listainfo Reserva, Listainfo PReserva);
void Menu_reserva2(Listainfo Reserva, Listainfo PReserva,Marcacao pessoa1);
void Menu_cancela(Listainfo Reserva, Listainfo PReserva);
void Menu_cancelaPR(Listainfo Reserva, Listainfo PReserva);
void Menu_cancelarR(Listainfo Reserva, Listainfo PReserva);
void Menu_utilizador(Listainfo Reserva,Listainfo PReserva);
/*------------------*/
/*Funcoes dos Files*/

void le_freservas(Listainfo Lista);
void le_fpreservas(Listainfo Lista);
void imprime_lista_fileR(Listainfo lista);
void imprime_lista_filePR(Listainfo lista);

/*Defesas*/
int defesa_nome(char* pessoa);
void uppertolower(char nome[MAX_NAME]);
int Checkifonlyint(char valor[]);
int Char_toint(char valor[]);
int Tamanho_Str(char numero[]);
int Defesa_Dia(Marcacao dia);
int Defesa_Hora(Data horai,Data horaf);
Marcacao CalculaTempoReal();

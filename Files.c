#include "struct_define.h"

void le_freservas(Listainfo Lista)
{
    FILE *ficheiro;
    int aux=0;
    Marcacao pessoa;
    ficheiro=fopen("Reservas.txt","r");
    if(ficheiro==NULL)
    {
        printf("File Reservas.txt: Not Found");
        exit(0);
    }
    else
        {
        while(aux==0)
        {
            if(fscanf(ficheiro,"%d:%d %d/%d/%d %d %ld ",&pessoa.horarioi.hora,&pessoa.horarioi.minutos,&pessoa.horarioi.dia,&pessoa.horarioi.mes,&pessoa.horarioi.ano,&pessoa.tipo,
                      &pessoa.telemovel)==EOF)
                {
                aux=1;
            }
            else{
                fgets(pessoa.nome,MAX_NAME,ficheiro);
                if(pessoa.nome[strlen(pessoa.nome)-1]=='\n')
                {
                    pessoa.nome[strlen(pessoa.nome)-1]='\0';
                }
                modifica_hora(&pessoa);
                insere_lista(Lista,pessoa);
            }
        }
    }
    fclose(ficheiro);
}

void le_fpreservas(Listainfo Lista)
{
    FILE *ficheiro;
    Marcacao pessoa;
    int aux=0;
    ficheiro=fopen("Pre-Reservas.txt","r");
    if(ficheiro==NULL)
    {
        printf("File Pre-Reservas.txt: Not Found");
    }
    else{
        while(aux==0)
        {
            if(fscanf(ficheiro,"%d:%d %d/%d/%d %d %ld ",&pessoa.horarioi.hora,&pessoa.horarioi.minutos,&pessoa.horarioi.dia,&pessoa.horarioi.mes,&pessoa.horarioi.ano,&pessoa.tipo,
                      &pessoa.telemovel)==EOF){
                aux=1;
            }
            else{
                fgets(pessoa.nome,MAX_NAME,ficheiro);
                if(pessoa.nome[strlen(pessoa.nome)-1]=='\n')
                {
                    pessoa.nome[strlen(pessoa.nome)-1]='\0';
                }
                modifica_hora(&pessoa);
                insere_lista(Lista,pessoa);
            }
        }
    }
    fclose(ficheiro);
}


void imprime_lista_fileR(Listainfo lista)
{
    FILE *ficheiro;
    Listainfo l;
    l=lista->next;
    ficheiro=fopen("Reservas.txt","w");
    while(l!=NULL){
    fprintf(ficheiro,"%d:%d %d/%d/%d %d %ld %s\n",l->info.horarioi.hora,l->info.horarioi.minutos,l->info.horarioi.dia,l->info.horarioi.mes,
            l->info.horarioi.ano,l->info.tipo,l->info.telemovel,l->info.nome);
    l=l->next;
    }
    fclose(ficheiro);
}

void imprime_lista_filePR(Listainfo lista)
{
    FILE *ficheiro;
    Listainfo l;
    l=lista->next;
    ficheiro=fopen("Pre-Reservas.txt","w");
    while(l!=NULL){
    fprintf(ficheiro,"%d:%d %d/%d/%d %d %ld %s\n",l->info.horarioi.hora,l->info.horarioi.minutos,l->info.horarioi.dia,l->info.horarioi.mes,
            l->info.horarioi.ano,l->info.tipo,l->info.telemovel,l->info.nome);
    l=l->next;
    }
    fclose(ficheiro);
}

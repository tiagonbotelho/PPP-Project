#include "struct_define.h"

Listainfo cria_lista(void)
{
    Listainfo aux;
    aux =(Listainfo)malloc(sizeof(Li_node));
    if(aux!=NULL){
        aux->info.telemovel=0;
        aux->info.horarioi.dia=0;
        aux->info.horarioi.mes=0;
        aux->info.horarioi.ano=0;
        aux->info.horarioi.hora=0;
        aux->info.horarioi.minutos=0;
        aux->info.horariof.dia=0;
        aux->info.horariof.mes=0;
        aux->info.horariof.ano=0;
        aux->info.horariof.hora=0;
        aux->info.horariof.minutos=0;
        aux->next=NULL;
        aux->prev=NULL;
    }
    return aux;
}

void insere_lista(Listainfo Lista, Marcacao it)
{
    Listainfo no,ant;
    no=(Listainfo)malloc(sizeof(Li_node));
    if(no!=NULL)
    {
        no->info = it;
        procura_lista(Lista,&ant,it);
        no->next=ant->next;
        if(no->next!=NULL)
        {
            (no->next)->prev=no;
        }
        ant->next=no;
        no->prev=ant;
    }

}


int verifica_data(Marcacao data1, Marcacao data2)
{
 long data1a, data2a;
    int hora1,hora2;
    data1a = data1.horarioi.dia + data1.horarioi.mes*100 + data1.horarioi.ano*10000;
    data2a = data2.horarioi.dia + data2.horarioi.mes*100 + data2.horarioi.ano*10000;
    hora1 = data1.horarioi.minutos + data1.horarioi.hora*100;
    hora2 = data2.horarioi.minutos + data2.horarioi.hora*100;
    if(data1a > data2a)
    {
        return 0;
    }
    else if(data1a <data2a)
    {
        return 1;
    }
    else
    {
        if(hora1>hora2)
        {
            return 0;
        }
        else if(hora1<hora2)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }
}

int compara_hora(Data hora1,Data hora2)
{
    int hora1a,hora2a;
    hora1a=hora1.minutos + hora1.hora*100;
    hora2a=hora2.minutos + hora2.hora*100;
    if(hora1a>hora2a){
        return 0;
    }
    else if(hora1a<hora2a)
    {
        return 1;
    }
    else{
        return 2;
    }
}

int compara_dia(Marcacao data1, Marcacao data2)
{
    long data1a, data2a;
    data1a = data1.horarioi.ano*10000 + data1.horarioi.mes*100 + data1.horarioi.dia;
    data2a = data2.horarioi.ano*10000 + data2.horarioi.mes*100 + data2.horarioi.dia;
    if(data1a > data2a)
    {
        return 0;
    }
    else if(data1a <data2a)
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

int compara_reserva(Marcacao reserva1, Marcacao reserva2)
{
    if(compara_dia(reserva1,reserva2)==2)
    {
        if(compara_hora(reserva1.horarioi,reserva2.horarioi)==2)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return 0;
    }
}

void procura_lista (Listainfo lista, Listainfo *ant, Marcacao data)
{
    Listainfo inutil=lista->next;
    *ant=lista;
    while(inutil!=NULL && verifica_data(inutil->info,data)==1)
    {
        *ant=inutil;
        inutil=inutil->next;

    }
}

void elimina_lista(Listainfo lista, Marcacao lixo)
{
    Listainfo l = lista->next;
    while(l!= NULL)
    {
        if(compara_reserva(lixo,l->info)==1)
        {
            (l->prev)->next = l->next;
            if(l->next != NULL)
            {
                (l->next)->prev = l->prev;
            }
            free(l);
        }
        l=l->next;
    }
}

void modifica_hora(Marcacao *hora)
{
    if(hora->tipo==LAVAGEM)
    {
        hora->horariof.minutos=hora->horarioi.minutos+30;
        if(hora->horarioi.minutos >=60)
        {
            hora->horariof.hora=hora->horarioi.hora+1;
            hora->horariof.minutos=hora->horariof.minutos % 60;
        }
        else{
            hora->horariof.hora=hora->horarioi.hora;
        }
    }
    else{
        hora->horariof.minutos=hora->horarioi.minutos;
        hora->horariof.hora=hora->horarioi.hora+1;
    }
    hora->horariof.ano=hora->horarioi.ano;
    hora->horariof.mes=hora->horarioi.mes;
    hora->horariof.dia=hora->horarioi.dia;
}

int disponibilidade(Marcacao nova, Marcacao fixa)
{
    if(compara_dia(nova,fixa)==2)
    {
        if(compara_hora(nova.horariof,fixa.horarioi)!=0 && compara_hora(nova.horarioi,fixa.horarioi)!=0)
        {
            return 0;
        }
        else if(compara_hora(nova.horarioi,fixa.horariof)!=1 && compara_hora(nova.horariof,fixa.horariof)!=1)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    else
    {
        return 0;
    }
}

int VerificaPossibilidade(Listainfo lista,Marcacao nova)
{
    Listainfo l=lista->next;
    while(l!=NULL)
    {
        if(disponibilidade(nova,l->info)==1)
        {
            return 1;
        }
        l=l->next;
    }
    return 0;
}

void imprime_lista(Listainfo lista)
{
    Listainfo l;
    l=lista->next;
    while(l!=NULL)
    {
        printf("----Reserva----\n");
        if(l->info.tipo==1){
            printf("Tipo:Lavagem\n");
        }
        else if(l->info.tipo==0)
        {
            printf("Tipo:Manutencao\n");
        }
        printf("Nome:%s\nTelemovel:%li\n",l->info.nome,l->info.telemovel);
        printf("%d/%d/%d e %d:%d\n",l->info.horarioi.dia,l->info.horarioi.mes,l->info.horarioi.ano,l->info.horarioi.hora,l->info.horarioi.minutos);
        printf("%d/%d/%d e %d:%d\n",l->info.horariof.dia,l->info.horariof.mes,l->info.horariof.ano,l->info.horariof.hora,l->info.horariof.minutos);
        printf("---------------\n\n");
        l=l->next;
    }
}

void numera_print(Listainfo Lista)
{
    Listainfo l = Lista->next;
    int i=1;
    while(l!=NULL)
    {
        printf("%d.\n",i);
        imprime_reserva(&(l->info));
        l=l->next;
        i++;
    }

}

Marcacao Procura_listanumerada(Listainfo Lista,int n)
{
    Listainfo l=Lista;
    int i;
    for(i=0;i<n;i++)
    {
        if(l!=NULL)
        {
            l=l->next;
        }
        else{
            break;
        }
    }
    return l->info;
}


void encaixa_prereservas(Listainfo PReserva,Marcacao reserva_eliminada,Listainfo Lista)
{
    Listainfo l=PReserva->next;
    while(l!=NULL)
    {
        if(VerificaPossibilidade(Lista,l->info)==0)
        {
            insere_lista(Lista,l->info);
            elimina_lista(PReserva,l->info);
        }
        else
        {
            l=l->next;
        }
    }
}

void imprime_reserva(Marcacao *marcacao)
{
    printf("Nome:%s \n",marcacao->nome);
    if(marcacao->tipo == 0)
        printf("Tipo: Manutencao\n");
    else
        printf("Tipo: Lavagem\n");

    printf("Numero de telemovel: %d\n",marcacao->telemovel);
    printf("Data: %d/%d/%d\n",marcacao->horarioi.dia,marcacao->horarioi.mes,marcacao->horarioi.ano);
    printf("Hora de inicio: %d:%d\n",marcacao->horarioi.hora,marcacao->horarioi.minutos);
    printf("Hora de fim: %d:%d\n\n",marcacao->horariof.hora,marcacao->horariof.minutos);
}

int imprimepnome(Listainfo Lista,Marcacao pessoa)
{
    Listainfo l=Lista->next;
    int aux=0;
    while(l!=NULL)
    {
        if(strcmp(pessoa.nome,l->info.nome)==0)
        {
            imprime_reserva(&(l->info));
            l=l->next;
            aux=1;
        }
        else{
            l=l->next;
        }
    }
    return aux;
}

void elimina_listatotal(Listainfo Lista)
{
    Listainfo l=Lista->next;
    Listainfo temp;
    while(l!=NULL)
    {
       temp=l;
       l=l->next;
       free(temp);
    }
}

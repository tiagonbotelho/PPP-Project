#include "struct_define.h"

int defesa_nome(char *nome)
{
    while(*nome!='\n')
    {
        if(isalpha(*nome)==0&&*nome!=32)
        {
            return 1;
        }
        else{
            nome+=1;
        }
    }
    return 0;
}

void uppertolower(char nome[])
{
    int i;
    while(nome[i]!='\0')
    {
        if(nome[i]>=65&&nome[i]<=90)
        {
            nome[i]+=32;
            i++;
        }
        else{
            i++;
        }
    }
}

Marcacao CalculaTempoReal()
{
    Marcacao tempor;
    struct tm *hora;
    time_t temp;
    time(&temp);
    hora = localtime(&temp);
    tempor.horarioi.hora = hora->tm_hour;
    tempor.horarioi.minutos = hora->tm_min;
    tempor.horarioi.dia = hora->tm_mday;
    tempor.horarioi.mes = hora->tm_mon + 1;
    tempor.horarioi.ano = hora->tm_year+1900;
    return tempor;

}

int Defesa_Dia(Marcacao dia)
{
    int dias_mes;
    Marcacao diareal = CalculaTempoReal();
    if(dia.horarioi.ano<0)
    {
        printf("Ano inv�lido.\n");
        return 0;
    }
    switch(dia.horarioi.mes)
    {
    case 1:
        {
            dias_mes=31;
            break;
        }
    case 2:
        {
            dias_mes = 28;
            break;
        }
    case 3:
        {
            dias_mes = 31;
            break;
        }
    case 4:
        {
            dias_mes = 30;
            break;
        }
    case 5:
        {
            dias_mes = 31;
            break;
        }
    case 6:
        {
            dias_mes = 30;
            break;
        }
    case 7:
        {
            dias_mes = 31;
            break;
        }
    case 8:
        {
            dias_mes = 31;
            break;
        }
    case 9:
        {
            dias_mes = 30;
            break;
        }
    case 10:
        {
            dias_mes = 31;
            break;
        }
    case 11:
        {
            dias_mes = 30;
            break;
        }
    case 12:
        {
            dias_mes = 31;
            break;
        }
    default:
        {
            printf("M�s Inv�lido.\n");
            return 0;
        }
    }
    if(dia.horarioi.dia<=0 || dia.horarioi.dia>dias_mes)
    {
        printf("Dia inv�lido.\n");
        return 0;
    }
    if(compara_dia(dia,diareal)!=0)
    {
        printf("Data inv�lido.\n");
        return 0;
    }
    return 1;
}

int Defesa_Hora(Data horai,Data horaf)
{
    if(horai.hora<8 || horai.hora>18)
    {
        return 0;
    }
    if(horai.minutos<0 || horai.minutos>59)
    {
        return 0;
    }
    if(horaf.hora>18)
    {
        return 0;
    }
    return 1;

}

int Tamanho_Str(char numero[])
{
    int n=0,i=0;
    while(numero[i]!='\n' && numero[i]!='\0')
    {
        i++;
        n++;
    }
    return n;
}
int Checkifonlyint(char numero[])
{
    int i,tam;
    tam = Tamanho_Str(numero);
    for(i=0;i<tam;i++)
    {
        if(isdigit(numero[i])==0)
        {
            return 0;
        }
    }
    return 1;
}

int Char_toint(char valor[])
{
  int num;
  num=atoi(valor);
  return num;
}
